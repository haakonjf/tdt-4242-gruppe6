[ -n "$PYENV_DEBUG" ] && set -x
export PYENV_ROOT="“/Users/sondre/.pyenv”"
program="$("/usr/local/opt/pyenv/bin/pyenv" which "${BASH_SOURCE##*/}")"
if [ -e "${program}" ]; then
  . "${program}" "$@"
fi
