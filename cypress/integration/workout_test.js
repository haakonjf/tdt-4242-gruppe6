describe('My First Test', () => {
  it('Create exercise validate difficulty, and edit and validate', () => {
    cy.visit('https://group6frontend.herokuapp.com/index.html')
    cy.contains('Log in').click()
    cy.get("form").children().first().type("admin").next().type("admin123").next().click().next().click()

    cy.wait(1000)
    cy.get('#nav-exercises').click({force:true})
    
    cy.wait(1000)
    cy.get('.container').find('input').click();

    cy.wait(1000)
    cy.get('form').children().first().type("mafakka").next().next().type("asd").next().next().type("rep").next().next().type(5)
    .next().next().type(5).next().next().get('Select').select('Arms').parent().next().type(2).next().next().children().first().click()
    cy.wait(1000)
    cy.contains('mafakka').click()
    cy.get('form').contains('Exercise difficulty').next().should('have.value', 2).parent().next().next().get('#btn-edit-exercise').click();
    cy.get('form').contains('Exercise difficulty').next().clear().type(5)
    cy.get('form').get('#btn-ok-exercise').click();
    
    cy.wait(1000)
    cy.get('#nav-exercises').click({force:true})
    cy.contains('mafakka').click()
    cy.wait(1000)
    cy.get('form').contains('Exercise difficulty').next().should('have.value', 5).parent().next().next().get('#btn-edit-exercise').click();
    cy.wait(1000)
    cy.get('form').get('#btn-delete-exercise').click();
    cy.wait(1000)

  })
})

describe('Test workout difficulty', () => {
  it('create workout validate difficulty', () => {
    cy.visit('https://group6frontend.herokuapp.com/index.html')
    cy.wait(1000)
    cy.contains('Log in').click()
    cy.get("form").children().first().type("admin").next().type("admin123").next().click().next().click()

    cy.wait(1000)
    cy.get('#nav-workouts').click({force:true})
    

    cy.wait(1000)
    cy.contains('temp').find('table').contains('0').should('match', (k, el) => {
      return (
        el.innerText.includes('0')
      )
    })

  })
})

describe('Test edit workout exercise, new difficulty value', () => {
  it('create workout validate difficulty', () => {
    cy.visit('https://group6frontend.herokuapp.com/index.html')
    cy.wait(1000)
    cy.contains('Log in').click()
    cy.get("form").children().first().type("admin").next().type("admin123").next().click().next().click()

    cy.wait(1000)
    cy.get('#nav-workouts').click({force:true})
    

    cy.wait(1000)
    cy.contains('temp').click();
    cy.wait(1000)
    cy.get('form').get('#btn-edit-workout').click();
    cy.get('form').get('#btn-add-exercise').click();
    cy.get(':nth-child(2) > :nth-child(3) > .form-select').select('asd')
    cy.get('#div-exercises > :nth-child(2) > :nth-child(5) > .form-control').clear().type(1)
    cy.get(':nth-child(2) > :nth-child(6) > .form-control').clear().type(1)
    cy.get('form').get('#btn-ok-workout').click();
    cy.wait(1000)

    cy.get('#nav-workouts').click({force:true})
    cy.wait(1000)
    cy.contains('temp').find('table').contains('0').should('match', (k, el) => {
      return (
        el.innerText.includes('0')
      )
    })
  })
})

describe('Test remove exercise, validate diffculty', () => {
  it('create workout validate difficulty', () => {
    cy.visit('https://group6frontend.herokuapp.com/index.html')
    cy.wait(1000)
    cy.contains('Log in').click()
    cy.get("form").children().first().type("admin").next().type("admin123").next().click().next().click()

    cy.wait(1000)
    cy.get('#nav-workouts').click({force:true})
    

    cy.wait(1000)
    cy.contains('temp').click();
    cy.wait(1000)
    cy.get('form').get('#btn-edit-workout').click();
    cy.get('form').get('#btn-remove-exercise').click();
    cy.wait(1000)
    cy.get('form').get('#btn-ok-workout').click();
    cy.wait(1000)
    cy.get('#nav-workouts').click({force:true})
    cy.wait(5000)
    cy.contains('temp').find('table').contains('0').should('match', (k, el) => {
      return (
        el.innerText.includes('0')
      )
    })
  })
})